const musicContainer = document.querySelector('.music_container')
const playBtn = document.querySelector('#play')
const progress = document.querySelector('.progress')
const progressContainer = document.querySelector('.progress-container')
const title = document.querySelector('#title')
const upload = document.querySelector('.upload')
const soundContainer = document.querySelector(".sound_bar_container")
const sound = document.querySelector(".sound_bar")


let dropZone = document.querySelector("#dropzone"),
  input = document.querySelector("#input_audio"),
  file,
  text,
  audio,
  frequencyArray;

// для визуализации
let C = document.querySelector("canvas"),
    $ = C.getContext("2d"),
    W = (C.width = innerWidth),
    H = (C.height = innerHeight),
    centerX = W / 2,
    centerY = H / 2,
    radius,
    // количество колонок
    bars = 200,
    x,
    y,
    xEnd,
    yEnd,
    // ширина колонки
    barWidth = 2,
    // высота колонки
    barHeight,
    // цвет колонки
    lineColor;

let slideIndex = 1;
showSlides(slideIndex);

var timer;
var time = 5000;

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("slide");
  let dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
}

dropZone.ondrop = (e) => {
    e.preventDefault();
  
    if (e.dataTransfer.items[0].kind == "file") {
      file = e.dataTransfer.items[0].getAsFile();
    } else return;
  
    playTrack(file);
};

dropZone.ondragover = (e) => {
    e.preventDefault();
};

dropZone.onclick = () => {
    input.click();
    input.onchange = () => {
      file = input.files[0];
  
      playTrack(file);
    };
};

function playTrack(file) {
    upload.style.display = "none";
    audio = new Audio();
    context = new AudioContext();
    analyser = context.createAnalyser();

    audio.src = URL.createObjectURL(file);
    source = context.createMediaElementSource(audio);
    source.connect(analyser);
    analyser.connect(context.destination);

    frequencyArray = new Uint8Array(analyser.frequencyBinCount);
    playSong();

    audio.loop = true;

    startAnimation();
}

function playSong() {
    musicContainer.classList.add('play');
    playBtn.querySelector('i.fa').classList.remove('fa-play');
    playBtn.querySelector('i.fa').classList.add('fa-pause');
    
    audio.play();
}

function pauseSong() {
    musicContainer.classList.remove('play');
    playBtn.querySelector('i.fa').classList.add('fa-play');
    playBtn.querySelector('i.fa').classList.remove('fa-pause');

    audio.pause();
}

playBtn.addEventListener('click', () => {
    const isPlaying = musicContainer.classList.contains('play');
    
    if (isPlaying) {
        pauseSong();
    } else {
        playSong();
    }
})

function setProgress(e) {
    const width = this.clientWidth;
    const clickX = e.offsetX
    const duration = audio.duration

    audio.currentTime = (clickX / width) * duration
}

function setVolume(e) {
    const width = this.clientWidth;
    const clickX = e.offsetX
    sound.style.width = `${(clickX / width) * 100}%`
    audio.volume = (clickX / width)
}


progressContainer.addEventListener('click', setProgress)
soundContainer.addEventListener('click', setVolume)



function setTimer() {
    timer = setInterval(function() {
        plusSlides(1);
    }, time);   // время задержки между слайдами в миллисекундах (в данном случае 5 секунд)
}

function resetTimer() {
    clearInterval(timer);
    setTimer();
}

setTimer()

function setTiming() {
    if (Number(document.getElementById('timing').value) == 0) {
        time = 5000;
    } else {
        time = Number(document.getElementById('timing').value) * 1000;
    }
    resetTimer();
}

//Визуализация
function startAnimation() {
    // включаем холст
    C.style.display = "block";

    let piece = audio.currentTime / audio.duration;

    // устанавливаем радиус круга
    // мы будем использовать два радиуса: один для прогресса, другой для визуализации частот
    radius = 105;
    
    // очищаем холст
    $.clearRect(0, 0, W, H);

    // копируем данные о частотах в frequencyArray
    analyser.getByteFrequencyData(frequencyArray);

    progress.style.width = `${piece * 100}%`

    // делаем итерацию по количеству колонок
    for (let i = 0; i < bars; i++) {
        // увеличиваем радиус
        radius = 240;

        // переводим количество колонок в радианы
        rads = Math.PI * 2 / bars;

        // определяем высоту колонок
        barHeight = frequencyArray[i] * 0.6;
        
        // двигаемся от 0 по часовой стрелке
        x = centerX + Math.cos(rads * i) * radius;
        y = centerY + Math.sin(rads * i) * radius;
        xEnd = centerX + Math.cos(rads * i) * (radius + barHeight);
        yEnd = centerY + Math.sin(rads * i) * (radius + barHeight);
        
        // рисуем колонки
        drawBar(x, y, xEnd, yEnd, barWidth, frequencyArray[i]);
    }

    // зацикливаем анимацию
    requestAnimationFrame(startAnimation);
}

function drawBar(x1, y1, x2, y2, width, frequency) {
    // цвет колонок меняется от светло-голубого до темно-синего
    lineColor = "rgb(" + frequency + ", " + frequency + ", " + 205 + ")";
    
    // рисуем линии
    $.strokeStyle = lineColor;
    $.lineWidth = width;
    $.beginPath();
    $.moveTo(x1, y1);
    $.lineTo(x2, y2);
    $.stroke();
}
